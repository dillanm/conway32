#ifndef __CellStruct__
#define __CellStruct__

enum status{DEAD, ALIVE};

struct Cell{

	int x, y;
	status state;

	Cell(){
		state = DEAD;
	}

	Cell(int inX, int inY){x = inX; y = inY; Cell();}

}

#endif