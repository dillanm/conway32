/*
	Created by Dillan Mann on 23/10/2014

	The sprite class is a 2D shape with an associated bitmap texture
	The draw method MUST be called before DisplayFrame

*/

#ifndef DJM_SPRITE_HPP
#define DJM_SPRITE_HPP

#include <Windows.h>

#include "Conway32\Vector2D.hpp"

namespace DJM{

	class Sprite
	{

		HBITMAP mBitmap; // stores the image associated with the sprite
		Vector2D mPosition, mSize, mOffset;

	public:

		Sprite(const Vector2D position, const Vector2D size);
		~Sprite(void);

		// Draws sprite
		void Draw(HDC& theHDC, HDC& BMPHDC);

		// loads a bitmap into object
		// return: true if succeed, false if fail
		bool LoadABitmap(const LPSTR szFileName); 

		// Accessors
		inline const DJM::Vector2D GetPosition(void) const { return mPosition; }
		inline const DJM::Vector2D GetSize(void) const { return mSize; }
		inline const DJM::Vector2D GetOffset(void) const { return mOffset; }

		// Mutators
		inline void SetPosition(const DJM::Vector2D pos) { mPosition = pos; }
		inline void SetSize(const DJM::Vector2D size) { mSize = size; }
		inline void SetOffset(const DJM::Vector2D offset) { mOffset = offset; }
	};
}

#endif