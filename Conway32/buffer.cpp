#include "buffer.h"

DJM::BufferHandler::BufferHandler(const int height, const int width)
	:M_BUFFERHEIGHT(height), M_BUFFERWIDTH(width)
{

	// Initialize buffers
	pBack = new Buffer(M_BUFFERHEIGHT, M_BUFFERWIDTH);
	pFront = new Buffer(M_BUFFERHEIGHT, M_BUFFERWIDTH);

	for (int i = 0; i < M_BUFFERHEIGHT; i++){
		for (int j = 0; j < M_BUFFERWIDTH; j++){
			pBack->bufData[i][j] = false;
			pFront->bufData[i][j] = false;
		}
	}

}

DJM::BufferHandler::~BufferHandler(void){

	// Safe pointer deallocation
	if (pFront != nullptr){ delete pFront; }
	if (pBack != nullptr){ delete pBack; }

}

void DJM::BufferHandler::SwapBuffers(void){

	// Store 1 buffer in a temporary object
	// And swap the others around
	Buffer *temp = pFront;
	*pFront = *pBack;
	*pBack = *temp;

}

void DJM::BufferHandler::ClearBack(void){
	// Set back buffer to all false values

	for (int i = 0; i < M_BUFFERHEIGHT; i++){
		for (int j = 0; j < M_BUFFERWIDTH; j++){
			pBack->bufData[i][j] = false;
		}
	}

}

void DJM::BufferHandler::AlternateFront(const int x, const int y){
	// Invert bool value in front buffer 

	pFront->bufData[y][x] = !pFront->bufData[y][x];
}

DJM::Buffer::Buffer(const int height, const int width)
:M_BUFFERWIDTH(width), M_BUFFERHEIGHT(height)
{
	// Similar to 2D array initialisation
	// Initialize the 1D vector then give
	// Each element it's own array
	bufData.resize(M_BUFFERHEIGHT);
	neighbours.resize(M_BUFFERHEIGHT);

	for (int i = 0; i < M_BUFFERHEIGHT; ++i){
		bufData[i].resize(M_BUFFERWIDTH);
		neighbours[i].resize(M_BUFFERWIDTH);
	}

	// Set to safe values after initialization
	for (int i = 0; i < M_BUFFERHEIGHT; ++i){
		for (int j = 0; j < M_BUFFERWIDTH; ++j){
			bufData[i][j] = false;
			neighbours[i][j] = 0;
		}
	}
}