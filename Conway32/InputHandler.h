/*
	Created by Dillan Mann, edited on 25/10/2014

	Input class is a helper class which encompasses input control
	from, theoretically, any type of environment. 

	Windows is the best advised option as it is confirmed to work on Windows.

*/


#ifndef DJM_INPUTHANDLER_HPP
#define DJM_INPUTHANDLER_HPP

#include <vector>

namespace DJM{

	
	namespace{
		struct Mouse{
			int x, y;
			bool click;
		};
	}


	class InputHandler{

		const int M_KEYBOARD_SIZE;

		// Keyboard array
		std::vector<bool> mKeys;

		// Mouse object
		Mouse mMouse;


	public:

		// Default constructors
		InputHandler(void);

		// Getters and setters various input devices
		void SetKey(const int key, const bool val);
		bool GetKey(const int key) const;

		void UpdateMouse(const int x, const int y);
		void GetMousePos(int &inX, int &inY) const;

		bool GetMouseClick(void);
		void SetMouseClick(const bool state){ mMouse.click = state; }
	};
}
#endif