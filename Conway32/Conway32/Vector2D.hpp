/*

Created by Dillan Mann on 23/10/2014

The Vector2D class is a 2-dimensional storage class
Could be templated for additional types but integer is fine for now

*/

#ifndef DJM_VECTOR2D_HPP
#define DJM_VECTOR2D_HPP


namespace DJM{
	class Vector2D{

	public:

		// Default constructors
		Vector2D(void);

		Vector2D(const int x, const int y);

		int x, y;

	};
}
#endif