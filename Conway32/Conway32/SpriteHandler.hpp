/*
	Created by Dillan Mann on 27/10/2014

	SpriteHandler is a manager class used to abstract sprite behaviour
	Handles creation and deletion of Sprite objects
	Also handles accessors/mutators to sprites

*/


#ifndef DJM_SPRITEHANDLER_HPP
#define DJM_SPRITEHANDLER_HPP

#include <vector>

#include "Vector2D.hpp"
#include "../Sprite.h"

namespace DJM{
	class SpriteHandler{

		// Internal sprite storage
		std::vector<std::vector<Sprite*>> mSprites;
		const int M_GRID_WIDTH, M_GRID_HEIGHT;

	public:

		// Must be handed grid size on creation
		SpriteHandler(const int gridWidth, const int gridHeight, const Vector2D size, const LPSTR szFileName);
		~SpriteHandler(void);

		// Accessor + mutator, does not specifically modify 
		// internal data but allows external modification
		Sprite *GetSprite(const int x, const int y) const;

	};
}

#endif