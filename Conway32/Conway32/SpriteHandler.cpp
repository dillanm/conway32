#include "SpriteHandler.hpp"

DJM::SpriteHandler::SpriteHandler(const int gridWidth, const int gridHeight, const Vector2D size, const LPSTR szFileName)
	:M_GRID_WIDTH(gridWidth), 
	M_GRID_HEIGHT(gridHeight)
{
	// resize first array to height
	mSprites.resize(M_GRID_HEIGHT);

	// resize each array of array to width
	for (int i = 0; i < M_GRID_HEIGHT; ++i){
		mSprites[i].resize(M_GRID_WIDTH);
	}

	for (int i = 0; i < M_GRID_HEIGHT; ++i){
		for (int j = 0; j < M_GRID_WIDTH; ++j){
			mSprites[i][j] = new Sprite(DJM::Vector2D(j * size.x, i * size.y), DJM::Vector2D(size.x, size.y));
			mSprites[i][j]->LoadABitmap(szFileName);
		}
	}

}

DJM::SpriteHandler::~SpriteHandler(void){

	for (int i = 0; i < M_GRID_HEIGHT; ++i){
		for (int j = 0; j < M_GRID_WIDTH; ++j){
			delete mSprites[i][j];
		}
	}
}

DJM::Sprite* DJM::SpriteHandler::GetSprite(const int x, const int y) const{
	return mSprites[x][y];
}