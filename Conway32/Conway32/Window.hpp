/*
	Created by Dillan Mann on 24/10/2014

	Window Class encompasses functionality of a Windows API window
	WndProc is a global function as it does not expect a 'this' and must either
	be made static or global.

	Windows can be instantiated but due to static members variables the instances
	will be identical except for window size.
*/



#ifndef DJM_WINDOW_HPP
#define DJM_WINDOW_HPP

#include <Windows.h>
#include <windef.h>

#include "../InputHandler.h"

LRESULT CALLBACK Proc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);


namespace DJM{
	class Window{

		// Window parameters
		static HBITMAP oldFrontBitMap, oldBackBitMap; // bitmaps to buffer between
		static HWND ghwnd; // window handle
		static RECT screenRect; // size of drawing window
		static HDC backHDC, frontHDC, bitmapHDC;	//Hardware device contexts

		int winWidth, winHeight;

	public:

		Window(void);
		Window(const int width, const int height);
		~Window(void);

		static InputHandler input;

		// Standard WINAPI calls
		static void Register(HINSTANCE hInstance);
		static bool Initialize(HINSTANCE hInstance, const int nCmdShow, const int width, const int height);
		static void SetBuffers(void);
		static void DisplayFrame(void);
		static void ReleaseResources(void);

		// Accessors / mutators
		static void UpdateMousePosition(const int x, const int y);

		HDC &GetBackHDC(void) const { return backHDC; }
		HDC &GetBitmapHDC(void) const { return bitmapHDC; }

		HWND GetWindowHandle(void) const { return ghwnd; }


	}; // end class definition
} // end namespace

#endif