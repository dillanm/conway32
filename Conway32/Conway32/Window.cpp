#include "Window.hpp"


// Static definitions --------------------------------------------------------------
HBITMAP	DJM::Window::oldFrontBitMap, DJM::Window::oldBackBitMap;
HWND DJM::Window::ghwnd;
RECT DJM::Window::screenRect;
HDC	DJM::Window::backHDC, DJM::Window::frontHDC, DJM::Window::bitmapHDC;

DJM::InputHandler DJM::Window::input;
// ----------------------------------------------------------------------------------
DJM::Window::Window(void){

}

DJM::Window::~Window(void){
}

DJM::Window::Window(const int width, const int height)
	:winWidth(width), 
	winHeight(height)
{
}


void DJM::Window::Register(HINSTANCE hInstance){

	// Standard WINAPI Window setup
	WNDCLASSEX  wcex;

	wcex.cbSize = sizeof (wcex);
	wcex.style = CS_HREDRAW | CS_VREDRAW; // Redraw if height or width changes
	wcex.lpfnWndProc = Proc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = 0;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);

	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = "Conway's Game of Life Simulation"; // Window title
	wcex.hIconSm = 0;

	RegisterClassEx(&wcex);
}

LRESULT CALLBACK Proc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam){

	switch (message)
	{
	case WM_CREATE:
		break;

	case WM_SIZE:
		DJM::Window::SetBuffers(); // if window resized; update screenrect so new window is fully drawn
		break;

	case WM_KEYDOWN:
		DJM::Window::input.SetKey(wParam, true);
		break;

	case WM_KEYUP:
		DJM::Window::input.SetKey(wParam, false);
		break;

	case WM_MOUSEMOVE:
		DJM::Window::input.UpdateMouse(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_LBUTTONUP:
		DJM::Window::input.SetMouseClick(true);
		break;

	case WM_PAINT:

		break;

	case WM_DESTROY:

		PostQuitMessage(0);

		break;
	}

	return DefWindowProc(hwnd, message, wParam, lParam);
}

void DJM::Window::SetBuffers(void){

		GetClientRect(ghwnd, &screenRect);				// Creates rect based on window client area
		frontHDC = GetDC(ghwnd);						// Initialises front buffer device context (window)
		backHDC = CreateCompatibleDC(frontHDC);			// sets up Back DC to be compatible with the front
		bitmapHDC = CreateCompatibleDC(backHDC);
		oldFrontBitMap = CreateCompatibleBitmap(frontHDC, screenRect.right, screenRect.bottom);	//creates bitmap compatible with the front buffer
		oldBackBitMap = (HBITMAP)SelectObject(backHDC, oldFrontBitMap); 						//creates bitmap compatible with the back buffer
		FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));

}

bool DJM::Window::Initialize(HINSTANCE hInstance, const int nCmdShow, const int width, const int height){

	// Initialize WINAPI Window Object

	HWND hwnd;
	hwnd = CreateWindow("Conway's Game of Life Simulation",
		"Conway's Game of Life Simulation",
		WS_SYSMENU,	// prevent window resizing
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		width,
		height,
		NULL,
		NULL,
		hInstance,
		NULL);

	if(!hwnd){
		return false;
	}


	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);
	ghwnd = hwnd;
	ShowCursor(true);
	return true;

}

void DJM::Window::DisplayFrame(void){

	BitBlt(frontHDC, screenRect.left, screenRect.top, screenRect.right,
		screenRect.bottom, backHDC, 0, 0, SRCCOPY);

	FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));

}

void DJM::Window::ReleaseResources(void){

	SelectObject(backHDC, oldBackBitMap);
	DeleteDC(backHDC);
	DeleteDC(bitmapHDC);
	ReleaseDC(ghwnd, frontHDC);

}