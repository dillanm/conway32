#include <iostream>
#include <Windows.h>
#include <process.h>
#include <vector>

#include "Sprite.h"
#include "InputHandler.h"
#include "Conway32\Window.hpp"
#include "buffer.h"
#include "Conway32\SpriteHandler.hpp"
using namespace std;

enum gameState{SELECT_TILES, SIMULATE};
gameState state = SELECT_TILES; // start in select mode


const int GRID_HEIGHT = 40,
		  GRID_WIDTH = 40;

const int ICON_SIZE = 20;

const int WIN_WIDTH = (GRID_WIDTH * ICON_SIZE) + (int)(ICON_SIZE * 0.5),
		  WIN_HEIGHT = (GRID_HEIGHT * ICON_SIZE) + (int)(ICON_SIZE * 1.33);


DJM::SpriteHandler spriteHandler(GRID_WIDTH, GRID_HEIGHT, DJM::Vector2D(ICON_SIZE, ICON_SIZE), "images/icons20.bmp");
DJM::BufferHandler bufferHandler(GRID_HEIGHT, GRID_WIDTH);
DJM::Window window;

void Simulate(const int x, const int y);
void Update(void);
void DrawGrid(void);
bool ValidIndex(const int x, const int y);
void GetMouseClick(void);
void UpdateGrid(void);

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,
                    PSTR szCmdLine, int nCmdShow)			
{									
    MSG	msg;

	// Window initialisation
	window.Register(hInstance);

   	if (!window.Initialize(hInstance, nCmdShow, WIN_WIDTH, WIN_HEIGHT)){
		return false;
	}

	window.SetBuffers();

	// Main loop
	while (true)					
    {				
		// Windows event loop
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)){
			if (msg.message == WM_QUIT){
				break;
			}

			TranslateMessage (&msg);							
			DispatchMessage (&msg);
		}

		else{

			Update();

			window.DisplayFrame();

		}
    }

	window.ReleaseResources();
	return msg.wParam ;										
}

void Update(void){

	if(state == SELECT_TILES){
		
		if(window.input.GetKey('P')){
			state = SIMULATE;
		}
	

		if (window.input.GetMouseClick()){
			GetMouseClick();
		}

 
	}
	else if(state == SIMULATE){

		for (size_t i = 0; i < GRID_HEIGHT; i++){
			for (size_t j = 0; j < GRID_WIDTH; j++){
				Simulate(i, j);
			}
		}

										// ---------------
		bufferHandler.SwapBuffers();	// volatile section, do not re-arrange
		Sleep(250);	
		bufferHandler.ClearBack();		// ---------------
	}

	UpdateGrid();
	DrawGrid();


}


void Simulate(const int x, const int y){
	
	size_t neighbours = 0;

	for(int i = -1; i < 2; i++){
		for(int j = -1; j < 2; j++){ // iterate through, finding compass point neighbours

			if (j == -1 && x == 0){  // prevent out of range indexing
				return;
			}
			else if(i == -1 && y == 0){
				return;
			}
			else if (j == 1 && x == 39){
				return;
			}
			else if (i == 1 && y == 39){
				return;
			}

			if(bufferHandler.GetFront().bufData[y+i][x+j] && ValidIndex(x+j, y+i) && !(i == 0 && j == 0)){
				neighbours++;
			}
		}
	}

	bufferHandler.GetBack().neighbours[y][x] = neighbours; 

	// bufData[][] returns bool; true = alive, false = dead
	if(bufferHandler.GetFront().bufData[y][x] && neighbours < 2){ // loneliness
		bufferHandler.GetBack().bufData[y][x] = false;
	}
	else if(bufferHandler.GetFront().bufData[y][x] && neighbours > 3){ // overcrowding
		bufferHandler.GetBack().bufData[y][x] = false; 
	}
	else if(bufferHandler.GetFront().bufData[y][x] && (neighbours == 2 || neighbours == 3)){ // keep alive
		bufferHandler.GetBack().bufData[y][x] = true;
	}
	else if((!bufferHandler.GetFront().bufData[y][x]) && neighbours == 3){ // reproduce
		bufferHandler.GetBack().bufData[y][x] = true;
	}

}

void DrawGrid(void){

	for (size_t i = 0; i < GRID_HEIGHT; i++){
		for (size_t j = 0; j < GRID_WIDTH; j++){
			spriteHandler.GetSprite(i, j)->Draw(window.GetBackHDC(), window.GetBitmapHDC());
		}
	}
}

bool ValidIndex(const int x, const int y){

	if (x < 0 || x > GRID_WIDTH){
		return false;
	}

	if (y < 0 || y > GRID_HEIGHT){
		return false;
	}

	return true;

}

void GetMouseClick(void){

	POINT p;
	GetCursorPos(&p);
	ScreenToClient(window.GetWindowHandle(), &p);

	// Divide screen into tiles to find
	// which tile was selected
	p.x /= ICON_SIZE;
	p.y /= ICON_SIZE;

	// Alternate buffers to show selected tile
	bufferHandler.AlternateFront(p.x, p.y);
}

void UpdateGrid(void){
	for (size_t i = 0; i < GRID_HEIGHT; i++){
		for (size_t j = 0; j < GRID_WIDTH; j++){

			// if alive, draw tile
			// uses spritesheet, x offset defines green or red tile
			if (bufferHandler.GetFront().bufData[i][j]){
				spriteHandler.GetSprite(i, j)->SetOffset(DJM::Vector2D(ICON_SIZE, 0));
			}
			// Otherwise don't draw tile
			else{
				spriteHandler.GetSprite(i, j)->SetOffset(DJM::Vector2D(0, 0));
			}
		}
	}
}