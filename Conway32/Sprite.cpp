#include "Sprite.h"

DJM::Sprite::Sprite(const Vector2D position, const Vector2D size)
	:mPosition(position), 
	mSize(size),
	mOffset(DJM::Vector2D(0, 0)),
	mBitmap(0)
{
}

DJM::Sprite::~Sprite(void){
}

void DJM::Sprite::Draw(HDC& theHDC, HDC& BMPHDC){
	// Needs to be passed the HDCs as no direct access


	BITMAP bmap;							// Get width and height of current bitmap
	GetObject(mBitmap, sizeof(bmap),&bmap); // Allows resizing of sprite / window

	HBITMAP originalBitMap = (HBITMAP)SelectObject(BMPHDC, mBitmap);

	// Blt the sprite onto the screen
	// Does not support transparency
	// Uses offset to determine which sprite from sheet to use
	BitBlt(theHDC, mPosition.x, mPosition.y, mSize.x, mSize.y, BMPHDC, mOffset.x, 0, SRCCOPY);
}

bool DJM::Sprite::LoadABitmap(const LPSTR szFileName){
	// Loads bitmap into member data

	// attempt to load bitmap
	mBitmap = (HBITMAP)LoadImage(NULL, szFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	// if load fails, show warning box
	if (mBitmap == NULL){
		return false;
	}

	return true;
		
}
