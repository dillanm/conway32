#include "InputHandler.h"

DJM::InputHandler::InputHandler(void)
	:M_KEYBOARD_SIZE(256)
{
	// Resize key vector to keyboard size
	mKeys.resize(M_KEYBOARD_SIZE);

	// Set all keys to safe false value
	for (std::vector<bool>::iterator iter = mKeys.begin(); iter != mKeys.end(); ++iter){
		*iter = false;
	}
}

void DJM::InputHandler::SetKey(const int key, const bool val){
	mKeys[key] = val;
}

bool DJM::InputHandler::GetKey(const int key) const{
	return mKeys[key];
}

void DJM::InputHandler::UpdateMouse(const int x, const int y){
	mMouse.x = x;
	mMouse.y = y;
}

void DJM::InputHandler::GetMousePos(int &inX, int &inY) const{
	inX = mMouse.x;
	inY = mMouse.y;
}

bool DJM::InputHandler::GetMouseClick(void){

	if (mMouse.click){
		// Reset to false before next check
		mMouse.click = false;
		return true;
	}

	return false;
}