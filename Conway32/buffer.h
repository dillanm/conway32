/*
	Created by Dillan Mann, edited on 27/10/2014
	
	BufferHandler and Buffer are classes to handle storage of
	a grid to simulate Conways Game of Life

	Buffer is initialized via a single overloaded constructor and has
	2 public storage containers; 1 boolean and 1 integer.

	Uses STD Vector containers.

	BufferHandler is a manager class which provides safe access to 
	the underlying buffers.

	See class declarations for specific documentation.

*/


#ifndef DJM_BUFFER_HPP
#define DJM_BUFFER_HPP

#include <vector>

namespace DJM{

	class Buffer{

		// Size of buffers, initialised in constructor
		int M_BUFFERWIDTH, M_BUFFERHEIGHT;

	public:

		// Only 1 overloaded constructor to initialize buffer size
		Buffer(const int height, const int width);

		// Underlying storage containers
		std::vector<std::vector<bool>> bufData;
		std::vector<std::vector<int>> neighbours;
	};

	class BufferHandler{

		const int M_BUFFERWIDTH, M_BUFFERHEIGHT;

		// Internal storage buffers
		Buffer *pFront, *pBack;


	public:

		BufferHandler(const int height, const int width);
		~BufferHandler(void);

		// Switch grid position between true/false
		void AlternateFront(const int x, const int y);

		// Return const pointer to front; read only
		const Buffer& GetFront(void) const { return *pFront; }

		// Give writeable back buffer to write changes to
		Buffer& GetBack(){ return *pBack; }

		// Swap buffer contents between each other
		// In reality it just swaps the pointers
		void SwapBuffers(void);

		// Clears back buffer to all false
		void ClearBack(void);


	};
}

#endif